package chapter4_factorypattern.factorymethod;

import chapter4_factorypattern.factorymethod.pizzastores.NYPizzaStore;

public class FactoryMethodMain {

    public static void main(String[] args) {
        PizzaStore pizzaStore = new NYPizzaStore();
        pizzaStore.orderPizza("cheese");
    }
}
