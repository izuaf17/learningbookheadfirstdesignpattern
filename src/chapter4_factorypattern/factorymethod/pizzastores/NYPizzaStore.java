package chapter4_factorypattern.factorymethod.pizzastores;

import chapter4_factorypattern.factorymethod.Pizza;
import chapter4_factorypattern.factorymethod.PizzaStore;
import chapter4_factorypattern.factorymethod.pizzastyles.NYStyleCheesePizza;

public class NYPizzaStore extends PizzaStore {
    @Override
    public Pizza createPizza(String type) {
        switch (type){
            case "cheese":
                return new NYStyleCheesePizza();
        }
        return null;
    }
}
