package chapter5_singletonpattern;

import util.PrintUtil;

public class Singleton {

    private static Singleton uniqueInstance = null;

    private Singleton(){}

    public static Singleton getInstance(){
        if (uniqueInstance == null){
            uniqueInstance = new Singleton();
        }
        return uniqueInstance;
    }

    public void singletonTest(){
        PrintUtil.print("I am Singleton Pattern");
    }
}
