package chapter5_singletonpattern;

public class SingletonPatternMain {

    public static void main(String[] args) {

        Singleton.getInstance().singletonTest();
    }
}
