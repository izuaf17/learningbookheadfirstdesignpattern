package chapter2_observerpattern;

public interface DisplayElement {

    void display();
}
