package chapter2_observerpattern;

public interface Observer {

    void update(float temperature, float humidity, float pressure);
}
