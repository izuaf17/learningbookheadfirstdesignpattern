package chapter2_observerpattern;

import chapter2_observerpattern.datas.WeatherData;
import chapter2_observerpattern.subscribers.CurrentConditionDisplay;

public class ObserverPatternMain {

    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        CurrentConditionDisplay currentConditionDisplay = new CurrentConditionDisplay(weatherData);

        //subscribe
        weatherData.registerObserver(currentConditionDisplay);
        weatherData.setMeasurements(10.0f,11.0f,12.0f);

        //unSubscribe
        weatherData.removeObserver(currentConditionDisplay);
        weatherData.setMeasurements(11.0f,12.0f,13.0f);

        //subscribe again
        weatherData.registerObserver(currentConditionDisplay);
        weatherData.setMeasurements(13.0f,14.0f,15.0f);
    }
}
