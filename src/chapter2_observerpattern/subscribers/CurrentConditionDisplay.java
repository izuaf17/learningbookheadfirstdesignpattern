package chapter2_observerpattern.subscribers;

import chapter2_observerpattern.DisplayElement;
import chapter2_observerpattern.Observer;
import chapter2_observerpattern.Subject;

public class CurrentConditionDisplay implements Observer, DisplayElement {

    private float temperature;

    private float humidity;

    private Subject weatherData;

    public CurrentConditionDisplay(Subject weatherData) {
        this.weatherData = weatherData;
    }

    @Override
    public void display() {
        System.out.println("Current Condition : " + temperature+ "F degrees and  "+ humidity+"% humidity");
    }

    @Override
    public void update(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        display();
    }
}
