package chapter3_decoratorpattern.beverage;

import chapter3_decoratorpattern.beverage.Beverage;

public class HouseBlend extends Beverage {

    public HouseBlend() {
        description = "House Blend Coffee";
    }

    @Override
    public double cost() {
        return .89;
    }
}
