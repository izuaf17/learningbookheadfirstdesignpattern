package chapter3_decoratorpattern;

import chapter3_decoratorpattern.beverage.Beverage;
import chapter3_decoratorpattern.beverage.Espresso;
import chapter3_decoratorpattern.beverage.HouseBlend;
import chapter3_decoratorpattern.condiment.Mocha;
import chapter3_decoratorpattern.condiment.Soy;
import chapter3_decoratorpattern.condiment.Whip;
import util.PrintUtil;

public class DecoratorPatternMain {

    public static void main(String[] args) {
        Beverage beverage = new Espresso();
        PrintUtil.print(beverage.getDescription() + " $" +beverage.cost());

        Beverage beverage2 = new HouseBlend();
        beverage2 = new Soy(beverage2);
        beverage2 = new Mocha(beverage2);
        beverage2 = new Whip(beverage2);

        PrintUtil.print(beverage2.getDescription() + " $" +beverage2.cost());
    }
}
