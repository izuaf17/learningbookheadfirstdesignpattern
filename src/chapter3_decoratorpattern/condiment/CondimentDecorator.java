package chapter3_decoratorpattern.condiment;

import chapter3_decoratorpattern.beverage.Beverage;

public abstract class CondimentDecorator extends Beverage {

    public abstract String getDescription();
}
