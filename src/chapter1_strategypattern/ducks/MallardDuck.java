package chapter1_strategypattern.ducks;

import chapter1_strategypattern.behaviors.FlyWithWingsBehavior;

public class MallardDuck extends Duck {

    public MallardDuck() {
        flyBehavior = new FlyWithWingsBehavior();
    }
}
