package chapter1_strategypattern.ducks;

import chapter1_strategypattern.behaviors.FlyBehavior;

public abstract class Duck  {

    FlyBehavior flyBehavior;

    public void setFlyBehavior(FlyBehavior flyBehavior){
        this.flyBehavior = flyBehavior;
    }

    public void performFly(){
        flyBehavior.fly();
    }
}
