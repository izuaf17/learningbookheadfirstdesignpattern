package chapter1_strategypattern.behaviors;

public interface FlyBehavior {
     void fly();
}
