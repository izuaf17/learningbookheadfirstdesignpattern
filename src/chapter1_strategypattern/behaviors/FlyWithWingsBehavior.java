package chapter1_strategypattern.behaviors;

public class FlyWithWingsBehavior implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("I'm Flying");
    }
}
