package chapter1_strategypattern.behaviors;

public class FlyWithRocketBehavior implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("I'm Flaying with Rocket");
    }
}
