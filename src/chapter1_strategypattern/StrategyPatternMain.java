package chapter1_strategypattern;

import chapter1_strategypattern.behaviors.FlyWithRocketBehavior;
import chapter1_strategypattern.ducks.Duck;
import chapter1_strategypattern.ducks.MallardDuck;

public class StrategyPatternMain {

    public static void main(String[] args) {
        Duck duck = new MallardDuck();
        duck.performFly();

        //dynamic change
        duck.setFlyBehavior(new FlyWithRocketBehavior());
        duck.performFly();
    }
}
