package chapter8_tamplatemethodpattern;

import util.PrintUtil;

public abstract class CaffeineBeverage {

    public final void prepareRecipe(){
        boilWater();
        brew();
        pourInCoup();
        addCondiments();
    }

    abstract void brew();

    abstract void addCondiments();

    private void boilWater(){
        PrintUtil.print("Boiling Water");
    }

    private void pourInCoup(){
        PrintUtil.print("Pouring into coup");
    }
}
