package chapter8_tamplatemethodpattern;

import util.PrintUtil;

public class Coffee extends CaffeineBeverage {

    @Override
    void brew() {
        PrintUtil.print("Dripping coffee through filter");
    }

    @Override
    void addCondiments() {
        PrintUtil.print("Adding sugar and milk");
    }
}
