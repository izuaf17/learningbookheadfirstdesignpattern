package chapter8_tamplatemethodpattern;

import util.PrintUtil;

public class Tea extends CaffeineBeverage {

    @Override
    void brew() {
        PrintUtil.print("Steeping the tea");
    }

    @Override
    void addCondiments() {
        PrintUtil.print("Adding Lemon");
    }
}
