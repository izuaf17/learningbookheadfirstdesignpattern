package chapter7_adapter_and_facadepattern.adapterpattern;

public class WildTurkey implements Turkey {
    @Override
    public void gobble() {
        System.out.println("Gobble Gobble");
    }

    @Override
    public void fly() {
        System.out.println("I'm Flying a short distance");
    }
}
