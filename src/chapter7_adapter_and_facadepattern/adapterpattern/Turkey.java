package chapter7_adapter_and_facadepattern.adapterpattern;

public interface Turkey {
    void gobble();
    void fly();
}
