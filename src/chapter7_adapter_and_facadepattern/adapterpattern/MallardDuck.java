package chapter7_adapter_and_facadepattern.adapterpattern;

public class MallardDuck implements Duck {
    @Override
    public void quack() {
        System.out.println("Quack");
    }

    @Override
    public void fly() {
        System.out.println("I'm Flying");
    }
}
