package chapter7_adapter_and_facadepattern.adapterpattern;

public interface Duck {
     void quack();
     void fly();
}
