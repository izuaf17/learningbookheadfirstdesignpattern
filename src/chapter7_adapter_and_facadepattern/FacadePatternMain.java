package chapter7_adapter_and_facadepattern;

import chapter7_adapter_and_facadepattern.facadepattern.Amplifier;
import chapter7_adapter_and_facadepattern.facadepattern.DvdPlayer;
import chapter7_adapter_and_facadepattern.facadepattern.Microphone;
import chapter7_adapter_and_facadepattern.facadepattern.SimpleHomeKaraokeFacade;

public class FacadePatternMain {

    public static void main(String[] args) {

        SimpleHomeKaraokeFacade simpleHomeKaraokeFacade =
                new SimpleHomeKaraokeFacade(new Amplifier(), new DvdPlayer(), new Microphone());

        simpleHomeKaraokeFacade.startKaraoke();
    }
}
