package chapter7_adapter_and_facadepattern;

import chapter7_adapter_and_facadepattern.adapterpattern.Duck;
import chapter7_adapter_and_facadepattern.adapterpattern.MallardDuck;
import chapter7_adapter_and_facadepattern.adapterpattern.TurkeyAdapter;
import chapter7_adapter_and_facadepattern.adapterpattern.WildTurkey;

public class AdapterPatternMain {

    public static void main(String[] args) {
        MallardDuck duck = new MallardDuck();
        WildTurkey turkey = new WildTurkey();

        Duck turkeyAdapter = new TurkeyAdapter(turkey);

        System.out.println("The Turkey says...");
        turkey.gobble();
        turkey.fly();

        System.out.println("The Duck says...");
        testDuck(duck);

        System.out.println("The TurkeyAdapter says...");
        testDuck(turkeyAdapter);
    }

    private static void testDuck(Duck duck){
        duck.quack();
        duck.fly();
    }
}
