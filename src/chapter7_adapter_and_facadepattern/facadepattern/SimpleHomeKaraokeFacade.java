package chapter7_adapter_and_facadepattern.facadepattern;

import util.PrintUtil;

public class SimpleHomeKaraokeFacade {

    private Amplifier amplifier;

    private DvdPlayer dvdPlayer;

    private Microphone microphone;

    public SimpleHomeKaraokeFacade(Amplifier amplifier, DvdPlayer dvdPlayer, Microphone microphone) {
        this.amplifier = amplifier;
        this.dvdPlayer = dvdPlayer;
        this.microphone = microphone;
    }

    public void startKaraoke(){
        amplifier.on();
        dvdPlayer.on();
        dvdPlayer.setVolume(9);
        dvdPlayer.installAmplifier(amplifier);
        microphone.on();
        amplifier.installMicrophone(microphone);
    }

    public void stopKaraoke(){
        amplifier.uninstallMicrophone();
        microphone.off();
        dvdPlayer.uninstallAmplifier();
        amplifier.off();
        dvdPlayer.off();
    }
}
