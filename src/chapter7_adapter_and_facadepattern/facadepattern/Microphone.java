package chapter7_adapter_and_facadepattern.facadepattern;

import util.PrintUtil;

public class Microphone {

    public void on(){
        PrintUtil.print("Turn On Microphone");
    }

    public void off(){
        PrintUtil.print("Turn Off Microphone");
    }
}
