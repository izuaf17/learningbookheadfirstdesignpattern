package chapter7_adapter_and_facadepattern.facadepattern;

import util.PrintUtil;

public class Amplifier {

    private Microphone microphone;

    public void on() {
        PrintUtil.print("Turn On Amplifier");
    }

    public void off(){
        PrintUtil.print("Turn Off Amplifier");
    }

    public void installMicrophone(Microphone microphone){
        if (microphone != null){
            this.microphone = microphone;
            PrintUtil.print("Microphone detected");
        } else {
            PrintUtil.print("No Microphone detected");
        }
    }

    public void uninstallMicrophone(){
        this.microphone = null;
        PrintUtil.print("Uninstall Microphone");
    }
}
