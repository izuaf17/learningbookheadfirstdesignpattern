package chapter7_adapter_and_facadepattern.facadepattern;

import util.PrintUtil;

public class DvdPlayer {

    private static final int MAX_VOLUME = 10;

    private Amplifier amplifier;

    public void on() {
        PrintUtil.print("Turn On DVD Player");
    }

    public void off() {
        PrintUtil.print("Turn Off DVD Player");
    }

    public void setVolume(int volume) {
        if (volume > MAX_VOLUME) {
            PrintUtil.print("Set Valume : " + 10);
        } else {
            PrintUtil.print("Set Valume : " + volume);
        }
    }

    public void installAmplifier(Amplifier amplifier) {
        if (amplifier != null) {
            this.amplifier = amplifier;
            PrintUtil.print("Amplifier detected");
        } else {
            PrintUtil.print("No Amplifier detected");
        }
    }

    public void uninstallAmplifier(){
        this.amplifier = null;
        PrintUtil.print("Uninstall Amplifier");
    }
}
