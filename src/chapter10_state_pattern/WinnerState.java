package chapter10_state_pattern;

import util.PrintUtil;

public class WinnerState implements State {

    private GumballMachine gumballMachine;

    public WinnerState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void insertQuarter() {
        PrintUtil.print("Please wait, we're already giving you a gumball");
    }

    @Override
    public void ejectQuarter() {
        PrintUtil.print("Sorry, you already turned the crank");
    }

    @Override
    public void turnCrank() {
        PrintUtil.print("Turning twice doesn't get you another gumball!");
    }

    @Override
    public void dispense() {
        PrintUtil.print("YOU'RE WINNER! you get two gumballs for your quarter");
        gumballMachine.releaseBall();
        if (gumballMachine.getCount() == 0){
            gumballMachine.setState(gumballMachine.getSoldOutState());
        } else {
            gumballMachine.releaseBall();
            if (gumballMachine.getCount() > 0){
                gumballMachine.setState(gumballMachine.getNoQuarterState());
            } else {
                PrintUtil.print("Oops, out of gumballs!");
            }
        }
    }
}
