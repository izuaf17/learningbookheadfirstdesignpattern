package chapter10_state_pattern;

import util.PrintUtil;

public class GumballMachine {

    private State soldOutState;
    private State noQuarterState;
    private State hasQuarterState;
    private State soldState;
    private State winnerState;

    private State state = soldOutState;
    private int count = 0;

    public GumballMachine(int count) {
        soldOutState = new SoldOutState(this);
        noQuarterState = new NoQuarterState(this);
        hasQuarterState = new HasQuarterState(this);
        soldState = new SoldState(this);
        winnerState = new WinnerState(this);
        this.count = count;
        if (this.count > 0) {
            state = noQuarterState;
        }
    }

    void insertQuarter() {
        state.insertQuarter();
    }

    void ejectQuarter() {
        state.ejectQuarter();
    }

    void turnCrank() {
        state.turnCrank();
        state.dispense();
    }

    void releaseBall() {
        PrintUtil.print("A gumball comes rolling out the slot...");
        if (count != 0) {
            count = count - 1;
        }
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getCount() {
        return count;
    }

    public State getSoldOutState() {
        return soldOutState;
    }

    public State getNoQuarterState() {
        return noQuarterState;
    }

    public State getHasQuarterState() {
        return hasQuarterState;
    }

    public State getSoldState() {
        return soldState;
    }

    public State getWinnerState() {
        return winnerState;
    }

    public void printStateRightNow() {
        String stateRightNow = "";
        if (state instanceof SoldOutState) {
            stateRightNow = "Sold Out State";
        } else if (state instanceof NoQuarterState) {
            stateRightNow = "No Quarter State";
        } else if (state instanceof HasQuarterState) {
            stateRightNow = "Has Quarter State";
        } else if (state instanceof SoldState) {
            stateRightNow = "Sold State";
        } else {
            stateRightNow = "Winner State";
        }
        PrintUtil.print("State : " +stateRightNow);
    }
}
