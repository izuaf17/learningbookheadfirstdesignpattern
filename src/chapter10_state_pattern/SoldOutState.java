package chapter10_state_pattern;


import util.PrintUtil;

public class SoldOutState implements State {

    private GumballMachine gumballMachine;

    public SoldOutState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void insertQuarter() {
        PrintUtil.print("You can't insert a quarter, the machine is sold out");
    }

    @Override
    public void ejectQuarter() {
        PrintUtil.print("You can't eject, the machine is sold out");
    }

    @Override
    public void turnCrank() {
        PrintUtil.print("You turned, but there are no gumballs");
    }

    @Override
    public void dispense() {
        PrintUtil.print("No gumball dispensed");
    }
}
