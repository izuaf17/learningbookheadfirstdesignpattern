package chapter10_state_pattern;

import util.PrintUtil;

public class NoQuarterState implements State {

    private GumballMachine gumballMachine;

    public NoQuarterState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void insertQuarter() {
        gumballMachine.setState(gumballMachine.getHasQuarterState());
        PrintUtil.print("You inserted a quarter");
    }

    @Override
    public void ejectQuarter() {
        PrintUtil.print("You haven't inserted a quarter");
    }

    @Override
    public void turnCrank() {
        PrintUtil.print("You turned but there's no quarter");
    }

    @Override
    public void dispense() {
        PrintUtil.print("You need to pay first");
    }
}
