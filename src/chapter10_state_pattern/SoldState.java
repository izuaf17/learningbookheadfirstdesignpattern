package chapter10_state_pattern;

import util.PrintUtil;

public class SoldState implements State {

    private GumballMachine gumballMachine;

    public SoldState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void insertQuarter() {
        PrintUtil.print("Please wait, we're already giving you a gumball");
    }

    @Override
    public void ejectQuarter() {
        PrintUtil.print("Sorry, you already turned the crank");
    }

    @Override
    public void turnCrank() {
        PrintUtil.print("Turning twice doesn't get you another gumball!");
    }

    @Override
    public void dispense() {
        PrintUtil.print("A gumball comes rolling out the slot");
        gumballMachine.releaseBall();
        if (gumballMachine.getCount() == 0){
            PrintUtil.print("Oops, out of gumballs!");
            gumballMachine.setState(gumballMachine.getSoldOutState());
        }else {
            gumballMachine.setState(gumballMachine.getNoQuarterState());
        }
    }
}
