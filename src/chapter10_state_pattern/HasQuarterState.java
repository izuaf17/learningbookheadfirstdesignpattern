package chapter10_state_pattern;

import util.PrintUtil;

import java.util.Random;

public class HasQuarterState implements State {

    private GumballMachine gumballMachine;

    private Random random = new Random(System.currentTimeMillis());

    public HasQuarterState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void insertQuarter() {
        PrintUtil.print("You can't insert another quarter");
    }

    @Override
    public void ejectQuarter() {
        PrintUtil.print("Quarter returned");
        gumballMachine.setState(gumballMachine.getNoQuarterState());
    }

    @Override
    public void turnCrank() {
        PrintUtil.print("You turned...");
        int winner = random.nextInt(10);
        if (winner == 0 && gumballMachine.getCount() > 0){
            gumballMachine.setState(gumballMachine.getWinnerState());
        } else{
            gumballMachine.setState(gumballMachine.getSoldState());
        }
    }

    @Override
    public void dispense() {
        PrintUtil.print("No gumball dispensed");
    }
}
