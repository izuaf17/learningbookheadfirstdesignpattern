package chapter10_state_pattern;

import util.PrintUtil;

public class StatePatternMain {

    private static final int GUMBALL_COUNT = 5;

    public static void main(String[] args) {
        GumballMachine gumballMachine = new GumballMachine(GUMBALL_COUNT);

        gumballMachine.printStateRightNow();
        PrintUtil.print("Gumball Start Count :" + gumballMachine.getCount());

        gumballMachine.insertQuarter();

        gumballMachine.printStateRightNow();
        gumballMachine.turnCrank();

        gumballMachine.printStateRightNow();
        PrintUtil.print("Gumball Count :" + gumballMachine.getCount());

        gumballMachine.insertQuarter();
        gumballMachine.ejectQuarter();
        gumballMachine.turnCrank();

        gumballMachine.printStateRightNow();
        PrintUtil.print("Gumball Count :" + gumballMachine.getCount());
    }
}
